/**
    Author:         Neeraj Patni
    Description:    We can use the class to fetch all the objects and fields information and dispaly them as picklist
    Why this?       There are situations when we have to fetch the information about all the objects or all the fields
                    of any particular objects to show on VF page. At that time this class will be useful.
                    
    How to User?
    1. Fetch all objects:          FetchSchemaData.getAllObjects();
    2. Fetch all fields:          FetchSchemaData.getAllFields( <Object API Name> );
**/
public with sharing class FetchSchemaData{
    
    
    /*
    * Fetch all the object names and sort them alphabatically. Store them in a SelectOption list and returns the same.
    */
    public static List<SelectOption> getAllObjects(){
        Map<String, SelectOption> sortMap = new Map<String, SelectOption>();
        List<String> sortList = new List<String>();
        
        for(Schema.sObjectType s : Schema.getGlobalDescribe().values()){
            Schema.DescribeSObjectResult objectDescribe = s.getDescribe();
            
            sortMap.put(objectDescribe.getLabel(), new SelectOption(objectDescribe.getLocalName(), objectDescribe.getLabel()));
            sortList.add(objectDescribe.getLabel());
        }
        
        sortList.sort();
        
        List<SelectOption> objectSelectoptions = new List<SelectOption>();
        
        for(String s : sortList){
            objectSelectoptions.add(sortMap.get(s));
        }
        
        return objectSelectoptions;
    }
    
    /*
    * Fetch all the field names of a specified object and sort them alphabatically. Store them in a 
    * SelectOption list and returns the same.
    */
    public static List<SelectOption> getAllFields(String objectName){
        Map<String, SelectOption> sortMap = new Map<String, SelectOption>();
        List<String> sortList = new List<String>();
        
        if(Schema.getGlobalDescribe().get(objectName) == null) return null;
        
        for(Schema.sObjectField f : Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().values()){
            Schema.DescribeFieldResult fieldDescribe = f.getDescribe();
            
            sortMap.put(fieldDescribe.getLabel(), new SelectOption(fieldDescribe.getLocalName(), fieldDescribe.getLabel()));
            sortList.add(fieldDescribe.getLabel());
        }
        
        sortList.sort();
        
        List<SelectOption> fieldSelectoptions = new List<SelectOption>();
        
        for(String s : sortList){
            fieldSelectoptions.add(sortMap.get(s));
        }
        
        return fieldSelectoptions;
    }
}